This is a webpage where the user can search or write a review/rate a particular hotel. When the user enters a number in the rating box (e.g. 5) and clicks on the Search button, a list of other reviews rated with 5 get extracted from the database and it is shown on the webpage.

Screenshot from the webpage:

![](Images/Search_Reviews.png)

![](Images/Show_Results.png)

The database where the list was extracted:

![](Images/Database.png)
