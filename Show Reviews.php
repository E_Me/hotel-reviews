<html>
<head>
  <title>The Stone Flower/Reviews</title>
  
  <style>
	body
	{
		background-color: #FAF0E6;
	}
	
	.rev
	{
		text-align: center;
		font-weight: bold;
		font-family: didot;
		font-style: oblique;
	}
</style>

</head>

<body>

<h1><div class = "rev"> SHOW RESULTS </h1></div>

<?php
  // create short variable names
  $searchtype=$_POST['searchtype'];
  $searchterm=trim($_POST['searchterm']);

  if (!$searchtype || !$searchterm) {
     echo 'You have not searched about anything. Go back to initial page.';
     exit;
  }

  if (!get_magic_quotes_gpc()){
    $searchtype = addslashes($searchtype);
    $searchterm = addslashes($searchterm);
  }

  @ $db = new mysqli('localhost', 'root', '', 'test');

  if (mysqli_connect_errno()) {
     echo 'Error! Could not connect with the database. Try again later.';
     exit;
  }

  $query = "select * from reviews where ".$searchtype." like '%".$searchterm."%'";
  $result = $db->query($query);

  $num_results = $result->num_rows;

  echo "<p><center> Total reviews: ".$num_results."</p>";

  for ($i=0; $i <$num_results; $i++) {
     $row = $result->fetch_assoc();
     echo "<br><strong><center>".($i+1).". Rating: ";
     echo htmlspecialchars(stripslashes($row['rating']));
     echo "</strong><br />";
	 
	 echo "<br /> Name: ";
	 echo $row['name'];
	 
	 echo "<br /> Comment: ";
	 echo $row['comment'];
     echo "</p></center>";
  }

  $result->free();
  $db->close();

?>

</body>
</html>
